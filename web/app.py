from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/<path:name>")
def renderPage(name):
    if("~" in str(name)):
	return render_template("index.html", nameHtml=403.html)
    elif("//" in str(name)):
	return render_template("index.html", nameHtml=403.html)
    elif(str(name[-5:]) == ".html"):
	try:
	    with open("/templates/"+str(name), 'r') as htmlPage:
		return render_template("index.html", nameHtml=name)
	except:
	    return render_template("index.html", nameHtml=404.html)
    elif(str(name[-4:]) == ".css"):
	try:
	    with open("/static/"+str(name), 'r') as cssPage:
		return render_template("index.html", nameHtml=name)
	except:
	    return render_template("index.html", nameHtml=404.html)
    else:
	return render_template("index.html", nameHtml=403.html)



if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
